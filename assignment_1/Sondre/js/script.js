// Variables used for the slide show
var slide_show_images = ["images/beer1.jpg", "images/beer2.jpg", "images/beer3.jpg"];
var slide_img = document.getElementById("slide-image");
var pause_start_button = document.getElementById("pause-start-button");
var slide_show_container = document.getElementById("slide-show-container");
var current_img = 0;
var img_opacity = 1;
var fade_out_int;
var fade_in_int;
var is_Paused = false;

// Functions for the slide show
function change_image() {
    current_img = current_img == slide_show_images.length - 1 ? 0 : current_img + 1;
    slide_img.src = slide_show_images[current_img];
}

function img_fade_out() {
    if (img_opacity > 0) {
        slide_img.style.opacity = img_opacity -= 0.01;
    } else {
        clearInterval(fade_out_int);
        change_image();
        start_fade_in();
    }
}

function img_fade_in() {
    if (img_opacity <= 1) {
        slide_img.style.opacity = img_opacity += 0.01;
    } else {
        clearInterval(fade_in_int);
        start_fade_out();
    }
}

function start_fade_out() {
    fade_out_int = setInterval(img_fade_out, 50);
}

function start_fade_in() {
    fade_in_int = setInterval(img_fade_in, 40);
}

function start_slide_show() {
    start_fade_out();
}

function pause_slide_show(){
    clearInterval(fade_out_int);
    clearInterval(fade_in_int);
}

pause_start_button.onclick = function (){
    if(!is_Paused) {
        img_opacity = 1;
        slide_img.style.opacity = img_opacity;
        pause_slide_show();
        is_Paused = true;
        pause_start_button.innerHTML = "&#9654;"
    } else {
        start_slide_show();
        is_Paused = false;
        pause_start_button.innerHTML = "&#9612;&#9612;"
    }
};

slide_show_container.onmouseover = function(){
    pause_start_button.style.display = "block";
};

slide_show_container.onmouseout = function(){
    pause_start_button.style.display = "none";
};

start_slide_show();



