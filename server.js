var express = require('express');
var path = require('path');
var app = express();

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
    //res.send('Hello World!');
});

app.use("/css", express.static(__dirname + '/css'));
app.use("/images", express.static(__dirname + '/images'));
app.use("/js", express.static(__dirname + '/js'));

app.listen(3000, function () {
    console.log('Takknemlighet listening on port 3000!');
});
